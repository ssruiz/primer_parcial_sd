/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author Samuel Sebastian Ruiz
 */
public class CiudadJson {

    public String objetoString(Ciudad m) {

        JSONObject obj = new JSONObject();
        JSONObject obj2 = new JSONObject();
        obj.put("id", m.getCodigo());
        obj.put("nombre", m.getNombre());
        obj.put("temperatura", m.getTemperatura());

        return obj.toJSONString();
    }

    public Ciudad stringObjeto(String str) throws Exception {

        Ciudad p = new Ciudad();
        JSONParser parser = new JSONParser();

        Object obj = parser.parse(str.trim());
        JSONObject jsonObject = (JSONObject) obj;
        double aux = (double) jsonObject.get("temperatura");
        p.setCodigo((String) jsonObject.get("id"));
        p.setNombre((String) jsonObject.get("nombre"));
        p.setTemperatura(aux);
        return p;
    }
}
