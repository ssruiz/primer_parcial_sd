/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author Samuel Sebastian Ruiz
 */
public class MensajeMetereologicoModel {

    private int id_estacion;
    private String ciudad;
    private String id_ciudad;

    public String getId_ciudad() {
        return id_ciudad;
    }

    public void setId_ciudad(String id_ciudad) {
        this.id_ciudad = id_ciudad;
    }
    private Long porcentaje_humedad;
    private Long temperatura;
    private Long velocidad_viento;
    private String fecha;
    private String hora;

    public int getId_estacion() {
        return id_estacion;
    }

    public void setId_estacion(int id_estacion) {
        this.id_estacion = id_estacion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public Long getPorcentaje_humedad() {
        return porcentaje_humedad;
    }

    public void setPorcentaje_humedad(Long porcentaje_humedad) {
        this.porcentaje_humedad = porcentaje_humedad;
    }

    public Long getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(Long temperatura) {
        this.temperatura = temperatura;
    }

    public Long getVelocidad_viento() {
        return velocidad_viento;
    }

    public void setVelocidad_viento(Long velocidad_viento) {
        this.velocidad_viento = velocidad_viento;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    @Override
    public String toString() {
        return "MensajeMetereologicoModel{" + "id_estacion=" + id_estacion + ", ciudad=" + ciudad + ", porcentaje_humedad=" + porcentaje_humedad + ", temperatura=" + temperatura + ", velocidad_viento=" + velocidad_viento + ", fecha=" + fecha + ", hora=" + hora + '}';
    }
    
    
}
