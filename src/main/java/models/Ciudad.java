/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author Samuel Sebastian Ruiz
 */
public class Ciudad {

    private String codigo;
    private String nombre;
    private double temperatura;

    public Ciudad(String codigo, String nombre, double temperatura) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.temperatura = temperatura;
    }
    
    public Ciudad(){
        codigo = "";
        nombre = "";
        temperatura = 0.0;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(double temperatura) {
        this.temperatura = temperatura;
    }

}
