/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author Samuel Sebastian Ruiz
 */
public class MensajeJMetSON {

    public String objetoString(MensajeMetereologicoModel m) {

        JSONObject obj = new JSONObject();
        JSONObject obj2 = new JSONObject();
        obj.put("id_estacion", m.getId_estacion());
        obj.put("ciudad", m.getCiudad());
        obj.put("porcentaje_humedad", m.getPorcentaje_humedad());
        obj.put("temperatura", m.getTemperatura());
        obj.put("velocidad_viento", m.getVelocidad_viento());
        obj.put("fecha", m.getFecha());
        obj.put("hora", m.getHora());
        obj.put("id_ciudad", m.getId_ciudad());
        return obj.toJSONString();
    }

    public MensajeMetereologicoModel stringObjeto(String str) throws Exception {
        MensajeMetereologicoModel p = new MensajeMetereologicoModel();
        JSONParser parser = new JSONParser();

        Object obj = parser.parse(str.trim());
        JSONObject jsonObject = (JSONObject) obj;
        Long aux = (Long)jsonObject.get("id_estacion");
        p.setId_estacion(aux.intValue());
        p.setCiudad((String) jsonObject.get("ciudad"));
        p.setPorcentaje_humedad((Long) jsonObject.get("porcentaje_humedad"));
        p.setTemperatura((Long) jsonObject.get("temperatura"));
        p.setVelocidad_viento((Long) jsonObject.get("velocidad_viento"));
        p.setFecha((String) jsonObject.get("fecha"));
        p.setHora((String) jsonObject.get("hora"));
        p.setId_ciudad((String) jsonObject.get("id_ciudad"));
        return p;
    }
}
