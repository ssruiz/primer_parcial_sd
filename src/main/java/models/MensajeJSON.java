/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author Samuel Sebastian Ruiz
 */
public class MensajeJSON {

    public String objetoString(Mensaje m) {

        JSONObject obj = new JSONObject();
        JSONObject obj2 = new JSONObject();
        obj.put("tipo", m.getTipo());
        obj.put("mensaje", m.getMensaje());

        return obj.toJSONString();
    }

    public Mensaje stringObjeto(String str) throws Exception {
        Mensaje p = new Mensaje();
        JSONParser parser = new JSONParser();

        Object obj = parser.parse(str.trim());
        JSONObject jsonObject = (JSONObject) obj;
        Long aux = (Long) jsonObject.get("tipo");
        p.setTipo(aux.intValue());
        p.setMensaje((String) jsonObject.get("mensaje"));
        return p;
    }
}
