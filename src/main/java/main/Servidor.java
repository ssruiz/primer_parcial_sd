/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import controllers.MensajeController;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import models.Ciudad;
import models.CiudadJson;
import models.Mensaje;
import models.MensajeJMetSON;
import models.MensajeJSON;
import models.MensajeMetereologicoModel;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author sam
 */
public class Servidor {

    HashMap<String, Ciudad> ciudades = new HashMap<String, Ciudad>();
    ArrayList<MensajeMetereologicoModel> datosCiudades = new ArrayList<MensajeMetereologicoModel>();

    public static void main(String[] a) {

        // Variables
        int puertoServidor = 9876;
        Servidor s = new Servidor();
       // s.inicializarCiudades();
        try {
            //1) Creamos el socket Servidor de Datagramas (UDP)
            DatagramSocket serverSocket = new DatagramSocket(puertoServidor);
            System.out.println("Servidor - UDP ");

            //2) buffer de datos a enviar y recibir
            byte[] receiveData = new byte[1024];
            byte[] sendData = new byte[1024];

            //3) Servidor siempre esperando
            while (true) {

                receiveData = new byte[1024];

                DatagramPacket receivePacket
                        = new DatagramPacket(receiveData, receiveData.length);

                System.out.println("Esperando a algun cliente... ");

                // 4) Receive LLAMADA BLOQUEANTE
                serverSocket.receive(receivePacket);

                System.out.println("________________________________________________");
                System.out.println("Se acepto un paquete");

                // Datos recibidos e Identificamos quien nos envio
                String datoRecibido = new String(receivePacket.getData());
                datoRecibido = datoRecibido.trim();
                System.out.println("DatoRecibido: " + datoRecibido);
                MensajeJSON mensaje = new MensajeJSON();
                Mensaje p = mensaje.stringObjeto(datoRecibido);

                InetAddress IPAddress = receivePacket.getAddress();

                int port = receivePacket.getPort();

                if (p.getTipo() == 1) {
                    MensajeMetereologicoModel mensajeMet = new MensajeMetereologicoModel();
                    MensajeJMetSON json = new MensajeJMetSON();

                    mensajeMet = json.stringObjeto(p.getMensaje());

                    System.out.println("Tipo Operacion 1 - Imprimiendo Mensaje Metereologico");
                    System.out.println(mensajeMet.toString());
                    s.datosCiudades.add(mensajeMet);
                    Ciudad c = new Ciudad(mensajeMet.getId_ciudad(), mensajeMet.getCiudad(), mensajeMet.getTemperatura().doubleValue());
                    s.ciudades.put(mensajeMet.getId_ciudad(), c);

                } else if (p.getTipo() == 2) {
                    System.out.println("Mensaje + " + p.getMensaje());

                    String codigoCiudad = p.getMensaje();

                    Ciudad ciudadSel = s.ciudades.get(codigoCiudad);
                    CiudadJson json = new CiudadJson();
                    String data = json.objetoString(ciudadSel);
                    sendData = data.getBytes();
                    DatagramPacket sendPacket
                            = new DatagramPacket(sendData, sendData.length, IPAddress, port);

                    serverSocket.send(sendPacket);

                } else if(p.getTipo()==3) {
                    String fecha = p.getMensaje();
                    for(MensajeMetereologicoModel mt: s.datosCiudades ){
                        
                    }
                }

            }

        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(1);
        }

    }

    // Simula un Api de temperaturas de ciudades
    private void inicializarCiudades() {
        Ciudad c = new Ciudad("101", "Luque", 35.0);
        ciudades.put("101", c);
        ciudades.put("102", new Ciudad("102", "Capiata", 36.0));
        ciudades.put("103", new Ciudad("103", "San Lorenzo", 30.0));
        ciudades.put("104", new Ciudad("104", "Asuncion", 32.0));
        ciudades.put("105", new Ciudad("105", "Fernando de la Mora", 29.0));

    }
}
