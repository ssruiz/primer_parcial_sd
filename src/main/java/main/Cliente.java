/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Scanner;
import models.Ciudad;
import models.CiudadJson;
import models.MensajeJMetSON;
import models.MensajeMetereologicoModel;
import models.Mensaje;
import models.MensajeJSON;

/**
 *
 * @author Samuel Sebastian Ruiz
 */
public class Cliente {

    public static void main(String a[]) throws Exception {

        // Datos necesario
        String direccionServidor = "localhost";

        if (a.length > 0) {
            direccionServidor = a[0];
        }

        int puertoServidor = 9876;

        try {

            BufferedReader inFromUser
                    = new BufferedReader(new InputStreamReader(System.in));

            DatagramSocket clientSocket = new DatagramSocket();

            InetAddress IPAddress = InetAddress.getByName(direccionServidor);
            //System.out.println("Intentando conectar a = " + IPAddress + ":" + puertoServidor + " via UDP...");
            Scanner reader = new Scanner(System.in);
            byte[] sendData = new byte[1024];
            byte[] receiveData = new byte[1024];
            System.out.println("Ingrese el tipo de operación ");

            Integer tipo = reader.nextInt();;

            if (tipo == 1) {
                System.out.println("Ingresado! ");
                Mensaje m = new Mensaje();
                m.setTipo(1);

                MensajeMetereologicoModel mensajeMet = new MensajeMetereologicoModel();
                MensajeJMetSON json = new MensajeJMetSON();
                System.out.println("Ingrese el id estacion: ");
                mensajeMet.setId_estacion(Integer.parseInt(inFromUser.readLine()));
                System.out.println("Ingrese el id ciudad: ");
                mensajeMet.setId_ciudad(inFromUser.readLine());
                System.out.println("Ingrese ciudad: ");
                mensajeMet.setCiudad(inFromUser.readLine());
                System.out.println("Ingrese porcentaje de humedad: ");
                mensajeMet.setPorcentaje_humedad(Long.parseLong(inFromUser.readLine()));
                System.out.println("Ingrese temperatura: ");
                mensajeMet.setTemperatura(Long.parseLong(inFromUser.readLine()));
                System.out.println("Ingrese velocidad del viento: ");
                mensajeMet.setVelocidad_viento(Long.parseLong(inFromUser.readLine()));
                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
                LocalDateTime now = LocalDateTime.now();
                //System.out.println(dtf.format(now));
                mensajeMet.setFecha(dtf.format(now));
                mensajeMet.setHora(new Date().toString());

                MensajeJSON mensajeJ = new MensajeJSON();
                String datoPaquete = json.objetoString(mensajeMet);

                m.setMensaje(datoPaquete);
                String datoEnviar = mensajeJ.objetoString(m);
                sendData = datoEnviar.getBytes();
                DatagramPacket sendPacket
                        = new DatagramPacket(sendData, sendData.length, IPAddress, puertoServidor);

                clientSocket.send(sendPacket);
            } else if (tipo == 2) {
                System.out.println("Ingrese el id ciudad: ");
                int id_ciudad = Integer.parseInt(inFromUser.readLine());
                Mensaje m = new Mensaje();
                MensajeJSON mensajeJ = new MensajeJSON();
                m.setTipo(2);
                m.setMensaje(String.valueOf(id_ciudad));
                String datoEnviar = mensajeJ.objetoString(m);
                sendData = datoEnviar.getBytes();
                DatagramPacket sendPacket
                        = new DatagramPacket(sendData, sendData.length, IPAddress, puertoServidor);

                clientSocket.send(sendPacket);
                System.out.println("Esperamos si viene la respuesta.");

                //Vamos a hacer una llamada BLOQUEANTE entonces establecemos un timeout maximo de espera
                clientSocket.setSoTimeout(10000);
                DatagramPacket receivePacket
                        = new DatagramPacket(receiveData, receiveData.length);

                try {
                    // ESPERAMOS LA RESPUESTA, BLOQUENTE
                    clientSocket.receive(receivePacket);

                    String respuesta = new String(receivePacket.getData());
                    CiudadJson json = new CiudadJson();
                    Ciudad ciudad = json.stringObjeto(respuesta);

                    InetAddress returnIPAddress = receivePacket.getAddress();
                    int port = receivePacket.getPort();

                    System.out.println("Respuesta desde =  " + returnIPAddress + ":" + port);
                    System.out.println("Temperatura de la ciudad de " + ciudad.getNombre() + " es: " + ciudad.getTemperatura() + "");

                } catch (SocketTimeoutException ste) {

                    System.out.println("TimeOut: El paquete udp se asume perdido.");
                }
                //clientSocket.close();

            } else {

            }

            clientSocket.close();
        } catch (UnknownHostException ex) {
            System.err.println(ex);
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }

}
